#include "test_class_prog.h"
#include <test_class_user.h>
#include <memory>

int main()
{
    TestClassUser user;
    auto obj = std::make_unique<TestClass>();
    user.pObj = obj.get();
    user.CallMethodA();
    user.CallMethodV();
    return 0;
}
