#include "test_class_prog.h"
#include <iostream>

int TestClass::MethodA()
{
    std::cout << "prog::TestClass::MethodA" << std::endl;
    return 15;
}

int TestClass::MethodV()
{
    std::cout << "prog::TestClass::MethodV" << std::endl;
    return 17;
}

TestClass::~TestClass()
{
}
