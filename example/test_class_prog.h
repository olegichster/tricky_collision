#pragma once

class TestClass {
    public:
        int MethodA();
        virtual int MethodV();
        virtual ~TestClass();
    private:
        const int value = 42;
};
