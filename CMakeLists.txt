CMAKE_MINIMUM_REQUIRED (VERSION 3.7)

set(TARGET_NAME example_project)

set (CMAKE_CXX_STANDARD 14)

project(${TARGET_NAME})

add_definitions(-Wno-trigraphs -Wall -Werror -Wextra -Wpedantic -Wnon-virtual-dtor -Wsuggest-override)
set(CMAKE_EXE_LINKER_FLAGS "-Wl,--fatal-warnings")

add_subdirectory(examplelib)
add_subdirectory(example)

