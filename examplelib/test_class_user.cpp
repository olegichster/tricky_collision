#include "test_class_user.h"
#include "test_class_lib.h"

void TestClassUser::CallMethodA()
{
    if (pObj) {
        pObj->MethodA();
    }
}

void TestClassUser::CallMethodV()
{
    if (pObj) {
        pObj->MethodV();
    }
}
