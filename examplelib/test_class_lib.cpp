#include "test_class_lib.h"
#include <iostream>

int TestClass::MethodA()
{
    std::cout << "lib::TestClass::MethodA, value=" << value << std::endl;
    return 5;
}

int TestClass::MethodV()
{
    std::cout << "lib::TestClass::MethodV" << std::endl;
    return 7;
}

TestClass::~TestClass()
{
}
